####Parte 1, Git####


**1.b	1. Software de control de versiones:**
		Un software (o programa) de control de versiones, es una aplicación originalmente ideada para, básicamente, ayudar a controlar mejor la evolución de un proyecto de trabajo y su historial; “Funciona como máquina del tiempo, que permite navegar a diferentes versiones de un proyecto en curso”.

	**2. Repositorios. Públicos/Privados**
		Que un repositorio sea público o privado significa, en el primer caso, que cualquier persona puede acceder a él, en contraste, los repositorios privados, sólo pueden ser accedidos por aquellos usuarios que posean las claves de autenticación apropiadas.

	**3. Arboles de Merkle**
		Es un método para estructurar información, que permite la verificación rápida de grandes cantidades de datos.
		Git utiliza éstos arboles para el manejo del control de versiones. La ventaja que aporta la utilización de éstos arboles, es que con la comparación de los hashes entre dos commit se podrían detectar si hay modificaciones o no.

	**4. Copia local y remota de los datos**
		El repositorio local, está compuesto por tres árboles administrados por git. El primero es el Directorio de trabajo, el cual contiene los archivos, el segundo, es el Index, y el último se llama head.
		A través del comando git add<filename> se pueden registrar cambios (añadirlos al Index). Se le puede realizar un commit, para registrar dichos cambios en el historial y agregar los cambios al “Head”. De ésta manera, se hace una copia local del trabajo.
		Para crear una copia remota, a través de un “Pull Request (PR)” se comunica a los compañeros de trabajo los cambios realizados en el proyecto. Una vez aprobado por éstos, se le aplica la función “Merge” para unir los cambios locales al repositorio remoto, generalmente, el “master”.

	**5. Que es un Commit**
		Un “commit” es el estado del proyecto en el que se trabaja, en un determinado momento de su historial. Incluye las últimas modificaciones que se han querido guardar en el proyecto, y, utilizándolos se puede “viajar en el tiempo” para volver a versiones anteriores del trabajo. Se deberían hacer los commits en los momentos que sean puntos clave, donde pueden surgir distintas versiones del trabajo a partir de él.

	**6. Como volver a una versión anterior del trabajo**
		Para volver a una versión anterior del trabajo, se puede utilizar el comando git checkout --<filename>, el cual reemplaza los cambios en el directorio de trabajo, con el último contenido del ”Head”.

	**7. Las branches (ramas) y para qué se usan**
		Las branches (ramas) son espacios independientes de la rama principal (master), o bifurcaciones de la misma. En ellas, se pueden realizar cambios al proyecto sin alterar los archivos guardados anteriormente en la rama master, o alguna otra rama.

####Parte 1, Markdown####


**2.b	1. Lenguaje Markdown**
		El lenguaje Markdown se dice que es un “lenguaje de marcado”. Lo que hace, básicamente, es facilitar la aplicación de formato a un texto, empleando ciertos caracteres.

	**2. Para qué se lo utiliza sus ventajas**
		Se lo utiliza para la elaboración de textos, que en principio estaban destinados a la web; por lo tanto ofrece una mayor rapidez y sencillez a la hora de elaborar dichos textos, que si se emplea directamente el lenguaje HTML. A pesar de que lo anterior es su uso más frecuente, también se lo puede emplear para cualquier tipo de texto, sin importar su destino.
