# SequenceDiagram
	participant esquema
	participant tema1
	participant master
	note right of master: Rama ppal
	master->>tema1:Nueva rama
	tema1-->>tema1: tema-1,texto de prueba
	tema1-->>master: checkout master
	master-->>tema1:checkout tema1
	tema1-->>tema1: segundo texto de prueba
	tema1->>master:merge tema1
	participant esquema
	master->>esquema: Nueva Rama
	esquema-->>esquema:diagrama uml ramas
	esquema-->>master:merge esquema