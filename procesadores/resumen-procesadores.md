### Organización básica de una computadora simple
#### Modelo de Von Neumann

El diseño básico de **von Neumann**, fue descrito por primera vez en 1945
por el matemático y físico **John von Neumann**.

Hoy en día, éste diseño, se conoce como **máquina de von Neumann**
y en dicha arquitectura, se basan la mayoría de las computadoras actuales.

La **máquina de von Neumann**, consiste de cinco partes básicas: 
	* memoria
	* unidad aritmética lógica (ALU, por sus siglas en inglés)
	* unidad de control
	* equipo periféricos de entrada y salida.

##### Organización de la CPU

La unidad de control, y la ALU, forman parte de la **CPU** (*Central Processing Unit*),
la cual es el *"cerebro"* de la computadora. Su función es ejecutar programas almacenados
en la memoria principal, buscando sus instrucciones, y examinándolas para después ejecutarlas
una otras otra. 

A demás de la unidad de control y la ALU, dentro de la CPU se encuentra una memoria pequeña 
y de alta velocidad, que sirve para almacenar resultados temporales y cierta información 
de control. Ésta memoria, se compone de varios **registros**, de los cuales cada uno tiene 
cierto tamaño y función. Cada registro puede contener un número, hasta algún máximo determinado
por el tamaño del registro.

El registro más importante, es el **contador de programa** (**PC**, *Program Counter*), que 
apunta a la siguiente instrucción que debe buscarse en la memoria para ejecutarse.
Otro registro importante, es el **registro de instrucciones** (**IR**, *Instruction Register*), 
que contiene la instrucción que se está ejecutando.

Todos éstos componentes están conectados entre sí por medio de un **bus**, que es una colección 
de alambres paralelos para transmitir direcciones, datos y señales de control.
Los buses, pueden también ser externos a la CPU, cuando la conectan a la memoria y a 
los dispositivos de Entrada/Salida (E/S)
El bus de datos permite el intercambio de datos entre la CPU  y el resto de unidades

La unidad de control se encarga de buscar instrucciones de la memoria principal y determinar 
su tipo. La ALU realiza operaciones como suma y AND booleano necesarias para ejecutar las 
instrucciones.

##### Memoria Principal

La **memoria principal**, es donde se almacenan temporalmente tanto los datos como los 
programas que la CPU debe procesar. Es inseparable de la CPU, con quen se comunica a través 
del bus de datos.
Consisten en varias **celdas** (o **localidades**), cada una de las cuales puede almacenar 
un elemento de información. Cada celda tiene un número, su **dirección**, con el cual los 
programas pueden referirse a ella. Si una memoria tiene *n* celdas, tendrán las direcciones 
de 0 a *n-1*. Todas las celdas contienen el mismo número de bits.

##### Dispositivos E/S

Los dispositivos de entrada y salida, como por ejemplo, el monitor, teclado, mouse, impresora, etc.
sirven para que el usuario pueda interactuar con el sistema de la computadora