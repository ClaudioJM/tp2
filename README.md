#Trabajo Práctico 2

###Amatista#
La **amatista** es una variedad macrocristalina violeta del cuarzo. El color puede ser más o menos intenso, según la cantidad de hierro que contenga. Puede presentarse coloreada por zonas con cuarzo otransparente o amarillo. Las puntas suelen ser más oscuras o degradarse hasta el cuarzo incoloro.
1. ####Características
A pesar de que es muy resistente a los ácidos, la amatista es muy susceptible al calor. De hecho, al calentarla a más de 300ºC cambia su color a café pardo, amarillo, anaranjado o verde, según su calidad y lugar de origen:
	* *450ºC:* se vuelve amarilla
	* *500ºC:* toma un color anaranjado fuerte (amatista quemada)
	* *600ºC:* se vuelve muy lechosa

2. ####Formación
La amatista es de orígen *magmático*. Se forma en filones con soluciones ricas en **óxidos de hierro**, que le dan su color morado característico.


